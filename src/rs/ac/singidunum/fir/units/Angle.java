package rs.ac.singidunum.fir.units;

/**
 * This class helps store and convert between different units used to express angles.
 *
 * @author Milan Tair
 */
public class Angle {
    final private double degrees;

    private Angle(double degrees) {
        this.degrees = degrees;
    }

    /**
     * Creates an object holding the value of angle whose magnitude was expressed in degrees
     * @param angleInDegrees The angle in degrees
     * @return An object of class Angle
     */
    public static Angle fromDegrees(double angleInDegrees) {
        return new Angle(angleInDegrees);
    }

    /**
     * Creates an object holding the value of angle whose magnitude was expressed in radians
     * @param angleInRadians The angle in radians
     * @return An object of class Angle
     */
    public static Angle fromRadians(double angleInRadians) {
        return new Angle(Math.toDegrees(angleInRadians));
    }

    /**
     * Creates an object holding the value of angle whose magnitude was expressed in gradients
     * @param angleInGradients The angle in gradients
     * @return An object of class Angle
     */
    public static Angle fromGradients(double angleInGradients) {
        return new Angle(angleInGradients * 9. / 10.);
    }

    /**
     * Creates an object holding the value of angle whose magnitude was expressed in minutes of ark
     * @param angleInMinutesOfArk The angle in minutes of ark
     * @return An object of class Angle
     */
    public static Angle fromMinutesOfArk(double angleInMinutesOfArk) {
        return new Angle(angleInMinutesOfArk / 60.);
    }

    /**
     * Creates an object holding the value of angle whose magnitude was expressed in seconds of ark
     * @param angleInSecondsOfArk The angle in seconds of ark
     * @return An object of class Angle
     */
    public static Angle fromSecondsOfArk(double angleInSecondsOfArk) {
        return new Angle(angleInSecondsOfArk / 3600.);
    }

    /**
     * Returns the stored angle's value expressed in degrees
     * @return the stored angle's value expressed in degrees
     */
    public double toDegrees() {
        return degrees;
    }

    /**
     * Returns the stored angle's value expressed in radians
     * @return the stored angle's value expressed in radians
     */
    public double toRadians() {
        return Math.toRadians(degrees);
    }

    /**
     * Returns the stored angle's value expressed in gradients
     * @return the stored angle's value expressed in gradients
     */
    public double toGradients() {
        return Math.toRadians(degrees * 10. / 9.);
    }

    /**
     * Returns the stored angle's value expressed in minutes of ark
     * @return the stored angle's value expressed in minutes of ark
     */
    public double toMinutesOfArk() {
        return Math.toRadians(degrees * 60.);
    }

    /**
     * Returns the stored angle's value expressed in seconds of ark
     * @return the stored angle's value expressed in seconds of ark
     */
    public double toSecondsOfArk() {
        return Math.toRadians(degrees * 3600.);
    }
}
