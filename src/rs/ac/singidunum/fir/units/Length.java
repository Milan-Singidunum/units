package rs.ac.singidunum.fir.units;

/**
 * This class helps store and convert between different units used to express length/distance.
 *
 * @author Milan Tair
 */
public class Length {
    final private double millimeters;

    private Length(double millimeters) {
        this.millimeters = millimeters;
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in nanometers
     * @param lengthInNanometers The length/distance in nanometers
     * @return An object of class Length
     */
    public static Length fromNanometers(double lengthInNanometers) {
        return new Length(lengthInNanometers * 1e-6);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in micrometers
     * @param lengthInMicrometers The length/distance in micrometers
     * @return An object of class Length
     */
    public static Length fromMicrometers(double lengthInMicrometers) {
        return new Length(lengthInMicrometers * 0.001);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in millimeters
     * @param lengthInMillimeters The length/distance in millimeters
     * @return An object of class Length
     */
    public static Length fromMillimeters(double lengthInMillimeters) {
        return new Length(lengthInMillimeters);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in decimeters
     * @param lengthInDecimeters The length/distance in decimeters
     * @return An object of class Length
     */
    public static Length fromDecimeters(double lengthInDecimeters) {
        return new Length(lengthInDecimeters * 10.);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in centimeters
     * @param lengthInCentimeters The length/distance in centimeters
     * @return An object of class Length
     */
    public static Length fromCentimeters(double lengthInCentimeters) {
        return new Length(lengthInCentimeters * 100.);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in meters
     * @param lengthInMeters The length/distance in meters
     * @return An object of class Length
     */
    public static Length fromMeters(double lengthInMeters) {
        return new Length(lengthInMeters * 1000.);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in kilometers
     * @param lengthInKilometers The length/distance in kilometers
     * @return An object of class Length
     */
    public static Length fromKilometers(double lengthInKilometers) {
        return new Length(lengthInKilometers * 1e6);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in inches
     * @param lengthInInches The length/distance in inches
     * @return An object of class Length
     */
    public static Length fromInches(double lengthInInches) {
        return new Length(lengthInInches * 25.4);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in feet
     * @param lengthInFeet The length/distance in feet
     * @return An object of class Length
     */
    public static Length fromFeet(double lengthInFeet) {
        return new Length(lengthInFeet * 304.8);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in yards
     * @param lengthInYards The length/distance in yards
     * @return An object of class Length
     */
    public static Length fromYards(double lengthInYards) {
        return new Length(lengthInYards * 914.4);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in miles
     * @param lengthInMiles The length/distance in miles
     * @return An object of class Length
     */
    public static Length fromMiles(double lengthInMiles) {
        return new Length(lengthInMiles * 1.609e6);
    }

    /**
     * Creates an object holding the value of length/distance whose magnitude was expressed in nautical miles
     * @param lengthInNauticalMiles The length/distance in nautical miles
     * @return An object of class Length
     */
    public static Length fromNauticalMiles(double lengthInNauticalMiles) {
        return new Length(lengthInNauticalMiles * 1.852e6);
    }

    /**
     * Returns the stored value of length/distance expressed in nanometers
     * @return the stored value of length/distance expressed in nanometers
     */
    public double toNanometers() {
        return millimeters / 1e-6;
    }

    /**
     * Returns the stored value of length/distance expressed in micrometers
     * @return the stored value of length/distance expressed in micrometers
     */
    public double toMicrometers() {
        return millimeters / 0.001;
    }

    /**
     * Returns the stored value of length/distance expressed in millimeters
     * @return the stored value of length/distance expressed in millimeters
     */
    public double toMillimeters() {
        return millimeters;
    }

    /**
     * Returns the stored value of length/distance expressed in decimeters
     * @return the stored value of length/distance expressed in decimeters
     */
    public double toDecimeters() {
        return millimeters / 10.;
    }

    /**
     * Returns the stored value of length/distance expressed in centimeters
     * @return the stored value of length/distance expressed in centimeters
     */
    public double toCentimeters() {
        return millimeters / 100.;
    }

    /**
     * Returns the stored value of length/distance expressed in meters
     * @return the stored value of length/distance expressed in meters
     */
    public double toMeters() {
        return millimeters / 1000.;
    }

    /**
     * Returns the stored value of length/distance expressed in kilometers
     * @return the stored value of length/distance expressed in kilometers
     */
    public double toKilometers() {
        return millimeters / 1e6;
    }

    /**
     * Returns the stored value of length/distance expressed in inches
     * @return the stored value of length/distance expressed in inches
     */
    public double toInches() {
        return millimeters / 25.4;
    }

    /**
     * Returns the stored value of length/distance expressed in feet
     * @return the stored value of length/distance expressed in feet
     */
    public double toFeet() {
        return millimeters / 304.8;
    }

    /**
     * Returns the stored value of length/distance expressed in yards
     * @return the stored value of length/distance expressed in yards
     */
    public double toYards() {
        return millimeters / 914.4;
    }

    /**
     * Returns the stored value of length/distance expressed in miles
     * @return the stored value of length/distance expressed in miles
     */
    public double toMiles() {
        return millimeters / 1.609e6;
    }

    /**
     * Returns the stored value of length/distance expressed in nautical miles
     * @return the stored value of length/distance expressed in nautical miles
     */
    public double toNauticalMiles() {
        return millimeters / 1.852e6;
    }
}
