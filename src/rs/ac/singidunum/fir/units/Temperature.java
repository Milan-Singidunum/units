package rs.ac.singidunum.fir.units;

/**
 * This class helps store and convert between different units used to express temperatures.
 *
 * @author Milan Tair
 */
public class Temperature {
    final private double kelvin;

    private Temperature(double kelvin) {
        this.kelvin = kelvin;
    }

    /**
     * Creates an object holding the value of temperature whose magnitude was expressed in degrees of Kelvin
     * @param temperatureInKelvin The temperature in degrees of Kelvin
     * @return An object of class Temperature
     */
    public static Temperature fromKelvin(double temperatureInKelvin) {
        return new Temperature(temperatureInKelvin);
    }

    /**
     * Creates an object holding the value of temperature whose magnitude was expressed in degrees of Celsius
     * @param temperatureInCelsius The temperature in degrees of Celsius
     * @return An object of class Temperature
     */
    public static Temperature fromCelsius(double temperatureInCelsius) {
        return new Temperature(temperatureInCelsius + 273.15);
    }

    /**
     * Creates an object holding the value of temperature whose magnitude was expressed in degrees of Fahrenheit
     * @param temperatureInFahrenheit The temperature in degrees of Fahrenheit
     * @return An object of class Temperature
     */
    public static Temperature fromFahrenheit(double temperatureInFahrenheit) {
        return new Temperature((temperatureInFahrenheit - 32.) * 5. / 9. + 273.15);
    }

    /**
     * Returns the stored temperatures's value expressed in degrees of Kelvin
     * @return the stored temperatures's value expressed in degrees of Kelvin
     */
    public double toKelvin() {
        return kelvin;
    }

    /**
     * Returns the stored temperatures's value expressed in degrees of Celsius
     * @return the stored temperatures's value expressed in degrees of Celsius
     */
    public double toCelsius() {
        return kelvin - 273.15;
    }

    /**
     * Returns the stored temperatures's value expressed in degrees of Fahrenheit
     * @return the stored temperatures's value expressed in degrees of Fahrenheit
     */
    public double toFahrenheit() {
        return (kelvin - 273.15) * 9. / 5. + 32.;
    }
}
