package rs.ac.singidunum.fir.units;

/**
 * This class helps store and convert between different units used to express mass.
 *
 * @author Milan Tair
 */
public class Mass {
    final private double grams;

    private Mass(double grams) {
        this.grams = grams;
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in tonnes
     * @param massInTones The mass in tonnes
     * @return An object of class Mass
     */
    public static Mass fromTonnes(double massInTones) {
        return new Mass(massInTones * 1e6);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in kilograms
     * @param massInKilograms The mass in kilograms
     * @return An object of class Mass
     */
    public static Mass fromKilograms(double massInKilograms) {
        return new Mass(massInKilograms * 1e3);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in grams
     * @param massInGrams The mass in grams
     * @return An object of class Mass
     */
    public static Mass fromGrams(double massInGrams) {
        return new Mass(massInGrams);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in milligrams
     * @param massInMilligrams The mass in milligrams
     * @return An object of class Mass
     */
    public static Mass fromMilligrams(double massInMilligrams) {
        return new Mass(massInMilligrams * 0.001);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in micrograms
     * @param massInMicrograms The mass in micrograms
     * @return An object of class Mass
     */
    public static Mass fromMicrograms(double massInMicrograms) {
        return new Mass(massInMicrograms * 1e-6);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in Imperial tons
     * @param massInImperialTons The mass in Imperial tons
     * @return An object of class Mass
     */
    public static Mass fromImperialTons(double massInImperialTons) {
        return new Mass(massInImperialTons * 1.016e6);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in US tons
     * @param massInUsTons The mass in US tons
     * @return An object of class Mass
     */
    public static Mass fromUsTons(double massInUsTons) {
        return new Mass(massInUsTons * 907185);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in stones
     * @param massInStones The mass in stones
     * @return An object of class Mass
     */
    public static Mass fromStones(double massInStones) {
        return new Mass(massInStones * 6350.29);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in pounds
     * @param massInPounds The mass in pounds
     * @return An object of class Mass
     */
    public static Mass fromPounds(double massInPounds) {
        return new Mass(massInPounds * 453.592);
    }

    /**
     * Creates an object holding the value of mass whose magnitude was expressed in ounces
     * @param massInOunces The mass in ounces
     * @return An object of class Mass
     */
    public static Mass fromOunces(double massInOunces) {
        return new Mass(massInOunces * 28.3495);
    }

    /**
     * Returns the stored value of mass expressed in tones
     * @return the stored value of mass expressed in tones
     */
    public double toTonnes() {
        return grams / 1e6;
    }

    /**
     * Returns the stored value of mass expressed in kilograms
     * @return the stored value of mass expressed in kilograms
     */
    public double toKilograms() {
        return grams / 1e3;
    }

    /**
     * Returns the stored value of mass expressed in grams
     * @return the stored value of mass expressed in grams
     */
    public double toGrams() {
        return grams;
    }

    /**
     * Returns the stored value of mass expressed in milligrams
     * @return the stored value of mass expressed in milligrams
     */
    public double toMilligrams() {
        return grams / 0.001;
    }

    /**
     * Returns the stored value of mass expressed in micrograms
     * @return the stored value of mass expressed in micrograms
     */
    public double toMicrograms() {
        return grams / 1e-6;
    }

    /**
     * Returns the stored value of mass expressed in Imperial tons
     * @return the stored value of mass expressed in Imperial tons
     */
    public double toImperialTons() {
        return grams / 1.016e6;
    }

    /**
     * Returns the stored value of mass expressed in US tons
     * @return the stored value of mass expressed in US tons
     */
    public double toUsTons() {
        return grams / 907185;
    }

    /**
     * Returns the stored value of mass expressed in stones
     * @return the stored value of mass expressed in stones
     */
    public double toStones() {
        return grams / 6350.29;
    }

    /**
     * Returns the stored value of mass expressed in pounds
     * @return the stored value of mass expressed in pounds
     */
    public double toPounds() {
        return grams / 453.592;
    }

    /**
     * Returns the stored value of mass expressed in ounces
     * @return the stored value of mass expressed in ounces
     */
    public double toOunces() {
        return grams / 28.3495;
    }
}
